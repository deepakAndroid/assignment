import React, {useState, useEffect} from 'react';
import {StatusBar} from 'react-native';
import {Provider} from 'react-redux';
import {store} from './app/store/store';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import LoginScreen from './app/screen/LoginScreen';
import HomeScreen from './app/screen/HomeScreen';
import {useTranslation} from 'react-i18next';
import './app/translation/i18n';
import {colors} from './app/utility/colors';
import SplashScreen from './app/screen/SplashScreen';

const Stack = createNativeStackNavigator();

const App = () => {
  const {t} = useTranslation();

  return (
    <>
      <StatusBar barStyle={'light-content'} />
      <Provider store={store}>
        <NavigationContainer>
          <Stack.Navigator
            screenOptions={{
              headerStyle: {backgroundColor: colors.primary800},
              headerTintColor: 'white',
              contentStyle: {backgroundColor: colors.primary700},
            }}>
            <Stack.Screen
              name="Splash"
              component={SplashScreen}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Login"
              component={LoginScreen}
              options={{
                title: t('loginPage.login'),
              }}
            />
            <Stack.Screen
              name="Home"
              component={HomeScreen}
              options={{
                title: t('homePage.home'),
              }}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </Provider>
    </>
  );
};

export default App;
