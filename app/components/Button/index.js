import React from 'react';
import {View, Pressable, Text} from 'react-native';
import styles from './styles';

const Button = ({children, onPress, mode, style,disabled}) => {
  return (
    <View style={style}>
      <Pressable
       disabled={disabled}
        onPress={onPress}
        style={({pressed}) => pressed && styles.pressed}>
        <View style={[styles.button,disabled ? {opacity: 0.5} : {opacity: 1}, mode === 'simple' && styles.simple]}>
          <Text
            style={[styles.buttonText, mode === 'simple' && styles.simpleText]}>
            {children}
          </Text>
        </View>
      </Pressable>
    </View>
  );
};

export default Button;
