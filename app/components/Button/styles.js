import { StyleSheet} from 'react-native'
import {colors} from '../../utility/colors'

export default StyleSheet.create({
    button: {
        borderRadius: 4,
        padding: 8,
        backgroundColor: colors.primary500,
      },
      simple: {
        backgroundColor: 'transparent',
      },
      buttonText: {
        color: 'white',
        textAlign: 'center',
        fontSize: 18
      },
      simpleText: {
        color: colors.primary200,
      },
      pressed: {
        opacity: 0.75,
        backgroundColor: colors.primary100,
        borderRadius: 4,
      },
})