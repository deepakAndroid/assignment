import React from 'react';
import {View, TextInput} from 'react-native';
import styles from './styles';

const Input = ({invalid, style, textInputConfig}) => {
  const inputStyles = [styles.input];

  if (invalid) {
    inputStyles.push(styles.invalidInput);
  }

  return (
    <View style={[styles.inputContainer, style]}>
      <TextInput style={[inputStyles]} {...textInputConfig} />
    </View>
  );
};

export default Input