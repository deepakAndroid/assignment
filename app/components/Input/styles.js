import {StyleSheet} from 'react-native'
import {colors} from '../../utility/colors'

export default StyleSheet.create({
    inputContainer: {
        marginHorizontal: 4,
        marginVertical: 8,
      },
      label: {
        fontSize: 12,
        color: colors.primary100,
        marginBottom: 4,
      },
      input: {
        backgroundColor: colors.primary100,
        color: colors.primary700,
        padding: 6,
        borderRadius: 6,
        fontSize: 18,
      },
      inputMultiline: {
        minHeight: 100,
        textAlignVertical: 'top',
      },
      invalidLabel: {
        color: colors.error500
      },
      invalidInput: {
        backgroundColor: colors.error50
      }
})