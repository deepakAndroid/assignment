import React from 'react';
import {View, Image, Text} from 'react-native';
import {BASE_IMAGE_URL} from '../../utility/util';
import styles from './styles';

const MovieItem = ({item}) => {
  return (
    <View style={styles.gridItem}>
      <View style={styles.innerContainer}>
        <Image
          style={styles.image}
          source={{
            uri: BASE_IMAGE_URL + item?.poster_path,
          }}
        />
        <Text style={styles.title}>{item?.title}</Text>
      </View>
    </View>
  );
};

export default MovieItem;
