import {StyleSheet, Platform} from 'react-native';
import {colors} from '../../utility/colors';

export default StyleSheet.create({
  gridItem: {
    flex: 1,
    margin: 16,
    height: 180,
    borderRadius: 8,
    elevation: 4,
    backgroundColor: 'white',
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 8,
    overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
  },
  innerContainer: {
    flex: 1,
    backgroundColor: colors.gray700,
    borderRadius: 8,
    overflow: 'hidden'
  },
  image: {
    flex: 1,
    overflow: 'hidden',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 14,
    marginBottom: 4,
    textAlign: 'center',
    color: 'white',
  },
});
