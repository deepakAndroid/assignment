import React, {useEffect} from 'react';
import {FlatList} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {fetchMovieList} from '../../store/movie-list';
import Loader from '../../components/Loader';
import ErrorText from '../../components/ErrorText';
import MovieItem from '../../components/MovieItem';
import '../../translation/i18n';
import {useTranslation} from 'react-i18next';

function renderMovieItem(itemData) {
  return <MovieItem item={itemData?.item} />;
}

const HomeScreen = () => {
  const {t} = useTranslation();
  const lang = useSelector(state => state.language.language);
  const movieList = useSelector(state => state.movieList.movieList);
  const isLoading = useSelector(state => state.movieList.isLoading);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchMovieList(lang));
  }, [lang]);

  if (isLoading === 1) {
    return <Loader />;
  }
  if (isLoading === 2 && movieList.length === 0) {
    return <ErrorText text={t('homePage.error')} />;
  }

  return (
    <FlatList
      data={movieList}
      keyExtractor={item => item.id}
      renderItem={renderMovieItem}
      numColumns={2}
    />
  );
};

export default HomeScreen;
