import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {View, I18nManager} from 'react-native';
import styles from './styles';
import Input from '../../components/Input';
import Button from '../../components/Button';
import {validateEmail, validatePassword} from '../../utility/util';
import {userAuthenticate} from '../../store/authenticateSlice';
import {availableLanguage, saveLoginDetail} from '../../utility/util';
import {useTranslation} from 'react-i18next';
import useLang from '../../translation/useLang';

const LoginScreen = ({navigation}) => {
  const [inputs, setInputs] = useState({
    email: {
      value: '',
      isValid: false,
    },
    password: {
      value: '',
      isValid: false,
    },
  });

  const language = useSelector(state => state.language.language);

  const dispatch = useDispatch();

  const {t, i18n} = useTranslation();
  const [availableLang, setAvailableLang] = useState(language);

  useLang(availableLang);

  function inputChangedHandler(inputIdentifier, enteredValue) {
    const isValid =
      inputIdentifier === 'email'
        ? validateEmail(enteredValue)
        : inputIdentifier === 'password'
        ? validatePassword(enteredValue)
        : false;
    setInputs(curInputs => {
      return {
        ...curInputs,
        [inputIdentifier]: {value: enteredValue, isValid: isValid},
      };
    });
  }

  function loginHandler() {
    const loginData = {
      email: inputs.email.value,
      password: inputs.password.value,
    };

    const emailIsValid =
      loginData.email.trim() !== '' && validateEmail(loginData.email);
    const passwordIsValid =
      loginData.password.trim() !== '' && validatePassword(loginData.password);

    if (!emailIsValid || !passwordIsValid) {
      setInputs(curInputs => {
        return {
          email: {value: curInputs.email.value.trim(), isValid: emailIsValid},
          password: {
            value: curInputs.password.value.trim(),
            isValid: passwordIsValid,
          },
        };
      });
      return;
    }
    navigation.reset({
      index: 0,
      routes: [{name: 'Home'}],
    });
    saveLoginDetail(
      true,
      loginData.email.trim(),
      loginData.password.trim(),
      language,
    ).then(res => {
      console.log('success');
      dispatch(
        userAuthenticate({
          isAuthenticate: true,
          email: loginData.email.trim(),
          password: loginData.password.trim(),
        }),
      );
    });
  }

  function languageHandler() {
    const languageCode =
      language === availableLanguage.english
        ? availableLanguage.arabic
        : availableLanguage.english;
    setAvailableLang(languageCode);
  }

  const strLang =
    language === availableLanguage.english
      ? t('loginPage.switchArabic')
      : t('loginPage.switchEnglish');

  return (
    <View style={styles.container}>
      <Input
        textInputConfig={{
          textContentType: 'emailAddress',
          keyboardType: 'email-address',
          placeholder: t('loginPage.email'),
          autoCorrect: false,
          autoCapitalize: 'none',
          onChangeText: inputChangedHandler.bind(this, 'email'),
          value: inputs.email.value,
          textAlign: I18nManager.isRTL ? 'right' : 'left',
        }}
      />

      <Input
        textInputConfig={{
          secureTextEntry: true,
          placeholder: t('loginPage.password'),
          maxLength: 15,
          onChangeText: inputChangedHandler.bind(this, 'password'),
          value: inputs.password.value,
          textAlign: I18nManager.isRTL ? 'right' : 'left',
        }}
      />

      <Button
        style={styles.button}
        onPress={loginHandler}
        disabled={!inputs.email.isValid || !inputs.password.isValid}>
        {t('loginPage.login')}
      </Button>
      <Button style={styles.language} onPress={languageHandler}>
        {strLang}
      </Button>
    </View>
  );
};

export default LoginScreen;
