import { StyleSheet } from 'react-native'
import {colors} from '../../utility/colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        padding: 24,
        justifyContent: 'center'
      },
      button: {
        minWidth: 120,
        marginHorizontal: 8,
      },
      language: {
        marginVertical: 16,
        minWidth: 120,
        marginHorizontal: 8,
      }
})