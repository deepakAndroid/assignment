import React, {useState, useEffect} from 'react';
import {StyleSheet, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import Loader from '../../components/Loader';
import {colors} from '../../utility/colors';
import {userAuthenticate} from '../../store/authenticateSlice';
import {getLoginDetail} from '../../utility/util';
import '../../translation/i18n';
import useLang from '../../translation/useLang';

const SplashScreen = ({navigation}) => {
  const lang = useSelector(state => state.language.language);
  const [availableLang, setAvailableLang] = useState(lang);
  const dispatch = useDispatch();
  const langChanged = useLang(availableLang);

  useEffect(() => {
    async function getDetail() {
      const detail = await getLoginDetail();
      if (detail?.isAuthenticated) {
        dispatch(
          userAuthenticate({
            isAuthenticate: detail?.isAuthenticated,
            email: detail?.email,
            password: detail?.password,
          }),
        );
        setAvailableLang(detail?.lang);
        if (langChanged) {
          navigation.reset({
            index: 0,
            routes: [{name: 'Home'}],
          });
        }
      } else {
        navigation.reset({
          index: 0,
          routes: [{name: 'Login'}],
        });
      }
    }
    getDetail();
  }, [langChanged]);
  return (
    <View style={styles.container}>
      <Loader />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary700,
  },
});

export default SplashScreen;
