import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  isAuthenticate: false,
  email: '',
  password: '',
};

export const authenticate = createSlice({
  name: 'authenticate',
  initialState: initialState,
  reducers: {
    userAuthenticate: (state, action) => {
      state.isAuthenticate = action.payload.isAuthenticate;
      state.email = action.payload.email;
      password = action.payload.password;
    },
  },
});

export const {userAuthenticate} = authenticate.actions;

export default authenticate.reducer;
