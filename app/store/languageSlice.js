import {createSlice} from '@reduxjs/toolkit';

const INITIAL_STATE = {
  language: 'en',
};

export const language = createSlice({
  name: 'language',
  initialState: INITIAL_STATE,
  reducers: {
    changeLanguage: (state, action) => {
      state.language = action.payload;
    },
  },
});

export const {changeLanguage} = language.actions;

export default language.reducer;
