import {createAsyncThunk} from '@reduxjs/toolkit';
import {BASE_URL, API_KEY} from '../utility/util';

export const fetchMovieList = createAsyncThunk('movie/movieList', async (lang) => {
  const response = await fetch(`${BASE_URL}${API_KEY}&language=${lang}&page=1`);
  if (!response.ok) throw new Error();
  const data = await response.json();
  return data;
});
