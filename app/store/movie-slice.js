import {createSlice} from '@reduxjs/toolkit';
import {fetchMovieList} from './movie-list';

const initialState = {
  errorMsg: '',
  isLoading: 0, //0: not started, 1: started: 2: completed
  movieList: [],
};

const movieSlice = createSlice({
  name: 'movieList',
  initialState: initialState,

  extraReducers: {
    [fetchMovieList.rejected]: (state, action) => {
      state.errorMsg = 'An error occured';
      state.isLoading = 0;
      state.movieList = [];
    },
    [fetchMovieList.pending]: state => {
      state.isLoading = 1;
      state.errorMsg = '';
      
    },
    [fetchMovieList.fulfilled]: (state, action) => {
      state.errorMsg = '';
      state.isLoading = 2;
      state.movieList = action.payload.results;
    },
  },
});

export const movieSliceAction = movieSlice.actions;
export default movieSlice.reducer;
