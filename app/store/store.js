import {configureStore} from '@reduxjs/toolkit';
import authenticateReducer from './authenticateSlice';
import movieSlice from './movie-slice';
import changeLanguage from './languageSlice';

export const store = configureStore({
  reducer: {
    authenticate: authenticateReducer,
    movieList: movieSlice,
    language: changeLanguage,
  },
});
