import React, {useState, useEffect} from 'react';
import './i18n';
import {useTranslation} from 'react-i18next';
import {useDispatch} from 'react-redux';
import {changeLanguage} from '../store/languageSlice';

const useLang = lang => {
  const [langChanged, setLangChanged] = useState(false);
  const dispatch = useDispatch();
  const {t, i18n} = useTranslation();

  useEffect(() => {
    async function changeLang() {
      i18n
        .changeLanguage(lang)
        .then(() => {
          dispatch(changeLanguage(lang));
          setLangChanged(true);
        })
        .catch(err => console.log(err));
    }
    changeLang();
  }, [lang]);
  return langChanged;
};

export default useLang;
