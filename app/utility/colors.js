export const colors = {
  primary100: '#c6affc',
  primary500: '#3e04c3',
  primary700: '#2d0689',
  primary800: '#200364',
  gray700: '#221c30',
};
