import AsyncStorage from '@react-native-async-storage/async-storage';
export const validateEmail = email => {
  const emailRegex =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return email.match(emailRegex) ? true : false;
};

export const validatePassword = password => {
  const passwordRegex = /^(?=.*[A-Z])(?=.*\W)(?!.* ).{8,15}$/;
  return password.match(passwordRegex) ? true : false;
};

export const availableLanguage = {
  english: 'en',
  arabic: 'ar',
};

export const saveLoginDetail = async (
  isAuthenticated,
  email,
  password,
  lang,
) => {
  const data = {
    isAuthenticated,
    email,
    password,
    lang,
  };
  AsyncStorage.setItem('USER_DETAIL', JSON.stringify(data), err => {
    if (err) {
      console.log('an error');
      throw err;
    }
  }).catch(err => {
    console.log('error is: ' + err);
  });
};

export const getLoginDetail = async () => {
  try {
    const data = await AsyncStorage.getItem('USER_DETAIL');
    if (data !== null) {
      return JSON.parse(data);
    }
  } catch (error) {
    // Error retrieving data
  }
};

export const BASE_URL =
  'https://api.themoviedb.org/3/movie/popular?api_key=';
export const API_KEY = '83fe93cdab042dc8f6f97430c6f17f53';
export const BASE_IMAGE_URL = 'https://image.tmdb.org/t/p/original';
